package io.demo.todo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.demo.todo.dto.TodoDto;
import io.demo.todo.entity.Todo;
import io.demo.todo.repository.TodoDAO;
import io.demo.todo.service.TodoService;

@SpringBootTest
public class TodoServiceTest {

	private final TodoService todoService;
	private final TodoDAO todoDao;

	@Autowired
	public TodoServiceTest(TodoService todoService, TodoDAO todoDao) {
		this.todoService = todoService;
		this.todoDao = todoDao;
	}

	@Test
	void checkfindAllTodo() {
		assertEquals(1, this.todoService.findAllTodo().size());
	}

	@Test
	void checkCreateAndGetTodo() {
		final String title = "title";
		final String content = "content";

		final TodoDto savedTodo = this.todoService.createTodo(new TodoDto(null, title, content, false));
		final Todo fetchedTodo = this.todoDao.findById(savedTodo.getId());

		assertEquals(title, fetchedTodo.getTitle());
		assertEquals(content, fetchedTodo.getContent());
		assertEquals(false, fetchedTodo.getDone());
		assertNotNull(fetchedTodo.getCreatedAt());
		assertNotNull(fetchedTodo.getUpdatedAt());
	}

	@Test
	void checkGetLastCreatedAndUpdateTodo() {
		final String title = "updated title";
		final String content = "upadted content";

		final Todo lastTodo = this.todoService.findLastCreatedTodo();

		this.todoService.updateTodo(new TodoDto(lastTodo.getId(), title, content, false));

		final Todo fetchedTodo = this.todoDao.findById(lastTodo.getId());
		assertEquals(title, fetchedTodo.getTitle());
		assertEquals(content, fetchedTodo.getContent());
		assertEquals(false, fetchedTodo.getDone());
		assertNotNull(fetchedTodo.getCreatedAt());
		assertNotNull(fetchedTodo.getUpdatedAt());
	}

	@Test
	void checkToggleDoneTodo() {
		final Todo lastTodo = this.todoService.findLastCreatedTodo();
		this.todoService.toggleDone(lastTodo.getId());

		final Todo fetchedTodo = this.todoDao.findById(lastTodo.getId());
		assertEquals(lastTodo.getTitle(), fetchedTodo.getTitle());
		assertEquals(lastTodo.getContent(), fetchedTodo.getContent());
		assertEquals(!lastTodo.getDone(), fetchedTodo.getDone());
		assertNotNull(fetchedTodo.getCreatedAt());
		assertNotNull(fetchedTodo.getUpdatedAt());
	}
}
