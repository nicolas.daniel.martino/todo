package io.demo.todo;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.demo.todo.repository.TodoDAO;
import io.demo.todo.service.TodoService;

@SpringBootTest
class TodoApplicationTests {

	private final TodoService todoService;
	private final TodoDAO todoDao;

	@Autowired
	public TodoApplicationTests(TodoService todoService, TodoDAO todoDao) {
		this.todoService = todoService;
		this.todoDao = todoDao;
	}

	@Test
	void contextLoads() {
		assertNotNull(this.todoService);
		assertNotNull(this.todoDao);
	}

	@Test
	void shouldDoDataImportAtStartup() {
		assertTrue(this.todoDao.count() > 0);
	}
}
