package io.demo.todo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import io.demo.todo.dto.TodoDto;
import io.demo.todo.entity.Todo;

@Mapper(componentModel = "spring")
public interface TodoMapper {
	TodoDto toDto(Todo todo);
	
	Todo merge(@MappingTarget Todo entity, TodoDto todoDto);
	
}
