package io.demo.todo.repository;
import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import io.demo.todo.entity.Todo;

public interface TodoDAO extends CrudRepository<Todo, Long>{

	public List<Todo> findAllByDoneTrue();
	public List<Todo> findAllByDoneFalse();
	public Todo findById(UUID id);
	public Todo deleteById(UUID id);
	public Todo findFirstByOrderByCreatedAtDesc();
}
