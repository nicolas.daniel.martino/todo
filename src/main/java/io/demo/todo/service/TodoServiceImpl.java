package io.demo.todo.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.demo.todo.dto.TodoDto;
import io.demo.todo.entity.Todo;
import io.demo.todo.mapper.TodoMapper;
import io.demo.todo.repository.TodoDAO;

@Service
public class TodoServiceImpl implements TodoService {

	private final TodoDAO todoDao;
	private final TodoMapper todoMapper;

	@Autowired
	public TodoServiceImpl(TodoDAO todoDao, TodoMapper todoMapper) {
		this.todoDao = todoDao;
		this.todoMapper = todoMapper;
	}

	@Override
	public TodoDto createTodo(TodoDto todoDto) {
		final Todo todo = this.todoMapper.merge(new Todo(), todoDto);
		return this.todoMapper.toDto(this.todoDao.save(todo));
	}

	@Override
	public TodoDto findTodoById(UUID id) {
		return this.todoMapper.toDto(this.todoDao.findById(id));
	}

	@Override
	public List<TodoDto> findAllTodo() {
		return StreamSupport.stream(this.todoDao.findAll().spliterator(), false).map(i -> this.todoMapper.toDto(i))
				.collect(Collectors.toList());
	}

	@Override
	public Todo findLastCreatedTodo() {
		return this.todoDao.findFirstByOrderByCreatedAtDesc();
	}

	@Override
	public Todo deleteTodoById(UUID id) {
		return this.todoDao.deleteById(id);
	}

	@Override
	public TodoDto updateTodo(TodoDto todoDto) {
		final Todo savedTodo = this.todoDao
				.save(this.todoMapper.merge(this.todoDao.findById(todoDto.getId()), todoDto));
		return this.todoMapper.toDto(savedTodo);
	}

	@Override
	public void toggleDone(UUID id) {
		final Todo todo = this.todoDao.findById(id);
		todo.setDone(!todo.getDone());
		this.todoDao.save(todo);
	}
}
