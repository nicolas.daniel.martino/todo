package io.demo.todo.service;

import java.util.List;
import java.util.UUID;

import io.demo.todo.dto.TodoDto;
import io.demo.todo.entity.Todo;

public interface TodoService {

	public TodoDto findTodoById(UUID id);

	public Todo deleteTodoById(UUID id);

	public void toggleDone(UUID id);

	public Todo findLastCreatedTodo();

	public TodoDto createTodo(TodoDto todoDto);

	public TodoDto updateTodo(TodoDto todoTo);

	public List<TodoDto> findAllTodo();
}
