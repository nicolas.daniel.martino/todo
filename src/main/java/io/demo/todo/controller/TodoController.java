package io.demo.todo.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.demo.todo.dto.TodoDto;
import io.demo.todo.service.TodoService;

@RestController
@RequestMapping("todo")
public class TodoController {

	private final TodoService todoService;

	public TodoController(TodoService todoService) {
		this.todoService = todoService;
	}

	@GetMapping(value = "/{id}", produces = "application/json")
	public TodoDto getTodo(@PathVariable UUID id) {
		return this.todoService.findTodoById(id);
	}

	@GetMapping(produces = "application/json")
	public List<TodoDto> getAllTodo() {
		return this.todoService.findAllTodo();
	}

	@PatchMapping(consumes = "application/json", produces = "application/json")
	public TodoDto updateTodo(@RequestBody TodoDto todoDto) {
		return this.todoService.updateTodo(todoDto);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public TodoDto createTodo(@RequestBody TodoDto todoDto) {
		return this.todoService.createTodo(todoDto);
	}

}
