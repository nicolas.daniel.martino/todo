package io.demo.todo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import io.demo.todo.entity.Todo;
import io.demo.todo.repository.TodoDAO;

@Component
public class DataInit implements ApplicationRunner {

	private final TodoDAO todoDao;

	private static final Logger logger = LoggerFactory.getLogger(DataInit.class);

	@Autowired
	public DataInit(TodoDAO todoDao) {
		this.todoDao = todoDao;
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		logger.debug("Start data import");
		if (this.todoDao.count() == 0) {
			final Todo a = new Todo();
			a.setTitle("Cooking dinner");
			a.setContent("Cooking fired rice according to ancient Chineese recipe");
			a.setDone(true);
			this.todoDao.save(a);
		}
		logger.debug("Finishest data import");

	}

}
