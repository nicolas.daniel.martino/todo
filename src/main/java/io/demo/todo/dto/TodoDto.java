package io.demo.todo.dto;

import java.util.UUID;

public class TodoDto {

	private UUID id;
	private String title;
	private String content;
	private Boolean done;
	
	
	public TodoDto() {
	}
	
	public TodoDto(UUID id, String title, String content, Boolean done) {
		super();
		this.id = id;
		this.title = title;
		this.content = content;
		this.done = done;
	}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Boolean getDone() {
		return done;
	}
	public void setDone(Boolean done) {
		this.done = done;
	}

	
	
}
